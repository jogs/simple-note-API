'use strinct';
(function(){
  var error = require('./errors');
  var success = require('./success');

  var resourceAPI = function(resource,id){
    return {
      readAll:function(req,res){
        resource.find({},function(err,docs){
          if (err) {
            error.mongoBadRequest(err,req,res)
          }
          else{
            // si no hay contenido
            if(docs.length===0){
              success.noContent(req,res);
            }
            // si hay contenido
            else{
              success.ok(req,res,docs);
            }
          }
        })
      },
      readOne:function(req,res){
        // si se paso el parametro id
        if(req.params[id]){
          resource.findById(req.params[id],function(err,doc){
            if (err) {
              error.mongoBadRequest(err,req,res)
            }
            else {
              // si existe el documento
              if(doc){
                success.ok(req,res,doc);
              }
              // si no existe el documento
              else{
                error.notFound(res,res);
              }
            }
          });
        }
        // si no existe el parametro id
        else{
          error.resourceBadRequest(req,res,"Falta el parametro: "+id)
        }
      },
      createOne:function(req,res){
        // si el mediaType es json
        if(req.is('json')){
          var newresource = resource(req.body);
          newresource.save(function(err,doc){
            if (err) {
              error.mongoBadRequest(err,req,res)
            }
            else{
              // si se creo el documento
              success.ok(req,res,doc);
            }
          })
        }
        // el cuerpo de la solicitud no es json
        else{
          error.mediaTypeJSON(req,res);
        }
      },
      updateOne:function(req,res){
        // si el mediaType es json
        if(req.is('json')){
          // si se paso el parametro id
          if(req.params[id]){
            resource.findByIdAndUpdate(req.params[id],{$set:req.body},function(err,doc){
              if (err) {
                error.mongoBadRequest(err,req,res)
              }
              else{
                // si existe el documento
                if(doc){
                  success.ok(req,res,doc);
                }
                // si no existe el documento
                else{
                  error.notFound(res,res);
                }
              }
            });
          }
          // si no existe el parametro id
          else{
            error.resourceBadRequest(req,res,"Falta el parametro: "+id)
          }
        }
        // el cuerpo de la solicitud no es json
        else{
          error.mediaTypeJSON(req,res);
        }
      },
      deleteOne:function(req,res){
        // si se paso el parametro id
        if(req.params[id]){
          resource.findById(req.params[id],function(err,doc){
            if (err){
              error.mongoBadRequest(err,req,res);
            }
            else{
              // si existe el documento
              if(doc){
                doc.remove(function(err,removed){
                  if (err) {
                    error.mongoBadRequest(err,req,res)
                  }
                  else{
                    // si se elimino
                    success.ok(req,res,removed);
                  }
                });
              }
              // si no existe el documento
              else{
                error.notFound(res,res);
              }
            }
          });
        }
        // si no existe el parametro id
        else{
          error.resourceBadRequest(req,res,"Falta el parametro: "+id)
        }
      }
    };
  }

  module.exports = resourceAPI;
})();
