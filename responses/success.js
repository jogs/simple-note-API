'use strict';
(function(){
  var logger = function(req,StatusCode,doc){
    console.log("\n-> Success "+StatusCode+"\n\tMethod: "+req.method+"\n\tPath: "+req.path+"\n\tBody: "+doc);
  }
  module.exports = {
    ok:function(req,res,doc){
      logger(req,200,doc);
      res.type('json')
      .status('200')
      .json({
        "success":200,
        "mensaje":"OK",
        "body":doc
      });
    },
    created:function(req,res,doc){
      logger(req,201,doc);
      res.type('json')
      .status('201')
      .json({
        "success":201,
        "mensaje":"Creado",
        "body":doc
      });
    },
    noContent:function(req,res){
      logger(req,204,{});
      res.type('json')
      .status('204')
      .json({
        "success":204,
        "mensaje":"Sin contenido",
        "body":{}
      });
    }
  };
})();
