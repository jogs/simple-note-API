'use strict';
(function(){
  var logger = function(req,StatusCode){
    console.error("\n-> Error "+StatusCode+"\n\tMethod: "+req.method+"\n\tPath: "+req.path);
  }
  module.exports = {
    mongoBadRequest:function(err,req,res){
      res.type('json')
      .status(400)
      .json({
        "error":400,
        "mensaje":"Solicitud erronea",
        "mongoDB":err
      });
      logger(req,400)
    },
    resourceBadRequest:function(req,res,msg){
      res.type('json')
      .status(400)
      .json({
        "error":400,
        "mensaje":"Solicitud erronea, Verifique la documentación\n"+msg
      });
      logger(req,400)
    },
    notFound:function(req,res){
  		res.type('json')
			.status(404)
			.json({
        "error":404,
        "mensaje":"No Encontrado"
      });
      logger(req,404)
  	},
    mediaTypeJSON:function(req,res){
      res.type('json')
      .status(415)
      .json({
        "error":415,
        "mensaje":"Media-Type no soportado, Se Esperaba JSON"
      });
      logger(req,415)
    },
    server:function(err,req,res,next) {
  		console.error(err.stack);
  		res.type('json')
			.status(500)
      .json({
        "error":500,
        "mensaje":"Error en el servidor"
      });
      logger(req,500)
  	}
  };
})();
