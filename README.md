# API NOTAS
## Índice

* [Especificaciones](#especificaciones-http)
  + [Métodos](##m%C3%A9todos "Métodos HTTP")
  + [Códigos de Estado](#c%C3%B3digos-de-estado "Códigos de Estado HTTP")
* [Recursos](#recursos)
  + [NOTE](#note)
      - [*Esquema](#note-schema)
  + [CATEGORY](#category)
      - [*Esquema](#category-schema)

## Especificaciones HTTP

### Métodos

| Método | CRUD | MongoDB |
|:-:|:-:|:-:|
| __POST__| _CREATE_ | ___INSERT___ |
| __GET__ | _READ_ | ___FIND___ |
| __PUT__| _UPDATE_ | ___UPDATE___ |
| __DETELE__ | _DELETE_ | ___REMOVE___ |

### Códigos de Estado

Códigos de Estado implementado en el API

| Tipo | Codigo | Significado |
| :-: | :-: | :-: |
| __SUCCESS__ | ___200___ | __OK__: _Solicitud Correcta_ |
| __SUCCESS__ | ___201___ | __CREATED__: _Entidad Creada_ |
| __SUCCESS__ | ___204___ | __NO CONTENT__: _Sin contenido (entidades)_ |
| __ERRORS__ | ___400___ | __BAD REQUEST__: _Error en la Solicitud_ |
| __ERRORS__ | ___404___ | __NOT FOUND__: _No encontrado (el recurso no existe)_ |
| __ERRORS__ | ___415___ | __UNSUPPORTED MEDIA TYPE__: _Media type (formato) no soportado_ |
| __ERRORS__ | ___500___ | __INTERNAL SERVER ERROR__: _Error interno_ |

## Recursos

### NOTE

#### NOTE SCHEMA

Esquema de NOTAS

    {
      "category_id":{"type":Schema.Types.ObjectId,ref:'Category',"required":true},
      "titulo":{"type":String,"required":true},
      "cuerpo":{"type":String,"required":true},
      "creado":{"type":Date,"default":Date.now()}
    }

### CATEGORY

#### CATEGORY SCHEMA

Esquema de categoria

    {
      "categoria":{"type":String,"required":true,"unique":true,"uppercase":true},
      "color":{"type":String,"required":true,"unique":true,"uppercase":true},
      "descripcion":{"type":String,"default":"Sin descripción"},
      "creado":{"type":Date,"default":Date.now()}
    }
