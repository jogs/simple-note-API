'use strict';
(function() {
	// Puerto en el que trabajara el api
	var port = process.env.PORT || 19170;
	// dependencias
	var express = require('express');
  var app = express();
	var bodyParser = require('body-parser');
	var mongoose = require('mongoose');

	// modelos
	var Category = require('./models/category');
	var Note = require('./models/note');
	// respuestas del api
	var _api={
		// respuestas a errores
		error:require('./responses/errors'),
		// respuesta correctas para notas
		note:require('./responses/resource')(Note,'note_id'),
		// respuestas correctas para categorias
		category:require('./responses/resource')(Category,'category_id')
	};
	// usar body-parser json para interpretar el cuerpo de las solicitudes
	app.use(bodyParser.json());

	// habilitar cors
	app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, Origin, X-Requested-With, Content-Type, Accept");
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
	});

	//conexion a mongodb
	mongoose.connect('mongodb://localhost:27017/myNotes');

	// rutas para cada recurso
	app.route('/categories')
	.get(_api.category.readAll);

	app.route('/category/:category_id?')
	.get(_api.category.readOne)
	.post(_api.category.createOne)
	.put(_api.category.updateOne)
	.delete(_api.category.deleteOne);

	app.route('/notes')
	.get(_api.note.readAll);

	app.route('/note/:note_id?')
	.get(_api.note.readOne)
	.post(_api.note.createOne)
	.put(_api.note.updateOne)
	.delete(_api.note.deleteOne);


	app
	// para toda solicitud a una ruta no definida usar _api.error.notFound
	.use(_api.error.notFound)
	// para toda solicitud que no tenga o genere un error usar _api.error.server
	.use(_api.error.server)
	// montar el servicio en el puerto definido
	.listen(port,function(){
		console.log('Express corriendo en el puerto '+port+';\n- Prisione Ctrl + C para detener el servicio\n- en un entorno de '+app.get('env'));
	});

  module.exports = app;
})();
