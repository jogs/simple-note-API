'use strict';
(function(){
  var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var note = new Schema({
    "category_id":{"type":Schema.Types.ObjectId,ref:'Category',"required":true},
    "titulo":{"type":String,"required":true},
    "cuerpo":{"type":String,"required":true},
    "creado":{"type":Date,"default":Date.now()}
  },{ collection : 'notes' });

  module.exports = mongoose.model('Note',note);
})();
