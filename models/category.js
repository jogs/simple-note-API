'use strict';
(function(){
  var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var category = new Schema({
    "categoria":{"type":String,"required":true,"unique":true,"uppercase":true},
    "color":{"type":String,"required":true,"unique":true,"uppercase":true},
    "descripcion":{"type":String,"default":"Sin descripción"},
    "creado":{"type":Date,"default":Date.now()}
  },{ collection : 'categories' });

  module.exports = mongoose.model('Category',category);
})();
